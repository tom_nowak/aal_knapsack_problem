import os
import random
import filecmp

random.seed()

for i in range(0, 10):
    n = str(random.randint(1, 15)) + " "
    W = str(random.randint(1, 50)) + " "
    for v_min in range(1,10):
        for delta_v in range(0,9):
            volume_max = str(v_min + delta_v) + " "
            volume_min = str(v_min) + " "
            os.system("../build/knapsack gen " + n + W + volume_min + volume_max + ">gen.txt")
            os.system("../build/knapsack solve brute <gen.txt >brute.txt")
            os.system("../build/knapsack solve dyn <gen.txt >dyn.txt")
            if(filecmp.cmp("brute.txt", "dyn.txt") == False):
                print("files are different - check gen.txt, brute.txt, dyn.txt")
                exit()

os.remove("gen.txt")
os.remove("brute.txt")
os.remove("dyn.txt")
