#brute:
#../build/knapsack test brute 20 1 1 5 1 25 1800 > data/brute_W1
#echo "test brute1 finished"
#../build/knapsack test brute 20 1 400 5 1 25 1800 > data/brute_W400
#echo "test brute2 finished"

#dyn - different W, small item volumes:
#../build/knapsack test dyn 4000 250 1 10 1 400 300 > data/dyn_W1_smallV
#echo "test1 finished"
#../build/knapsack test dyn 1500 50 10 10 1 400 900 > data/dyn_W10_smallV
#echo "test2 finished"
#../build/knapsack test dyn 500 50 100 10 1 400 900 > data/dyn_W100_smallV
#echo "test3 finished"

#dyn - different W, large item volumes:
#../build/knapsack test dyn 4000 250 1 10 400 20000 300 > data/dyn_W1_largeV
#echo "test4 finished"
#../build/knapsack test dyn 1500 50 10 10 400 20000 900 > data/dyn_W10_largeV
#echo "test5 finished"
#../build/knapsack test dyn 500 50 100 10 400 20000 900 > data/dyn_W100_largeV
#echo "test6 finished"

#../build/knapsack test dyn 20 1 100000 10 400 20000 900 > data/dyn_W100000
