import os
import numpy
import matplotlib.pyplot as plt

#based on: https://matplotlib.org/gallery/api/two_scales.html
def two_scales(ax1, n, data1, data2, c1, c2):
    ax2 = ax1.twinx()
    ax1.plot(n, data1, color=c1)
    ax1.set_xlabel('n', rotation=0)
    ax1.set_ylabel('t(n) [ms]')
    ax2.plot(n, data2, color=c2)
    plt.ylim(0,2)
    ax2.set_ylabel('q(n)')
    return ax1, ax2

#based on: https://matplotlib.org/gallery/api/two_scales.html
def color_y_axis(ax, color):
    for t in ax.get_yticklabels():
        t.set_color(color)
    return None

for name in os.listdir("data"):
    arr = numpy.loadtxt("data/" + name, skiprows = 1)
    _, ax = plt.subplots()
    ax1, ax2 = two_scales(ax, arr[:,0], arr[:,1], arr[:,2], 'b', 'g')
    color_y_axis(ax1, 'b')
    color_y_axis(ax2, 'g')
    plt.title(name)
    plt.tight_layout()
    plt.savefig("images/" + name + ".png")
    plt.close()
