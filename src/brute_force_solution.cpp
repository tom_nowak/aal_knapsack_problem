#include "brute_force_solution.h"
#include "data.h"

namespace
{
    class ItemSubset
    {
    public:
        ItemSubset(const knapsack_problem::Data &d)
            : data(d), vector(d.getNumberOfItems(), 0), totalValue(0), totalVolume(0)
        {}

        bool nextSubset()
        {
            for(unsigned i = 0; i < vector.size(); ++i)
            {
                if(vector[i] == 0)
                {
                    // add item i to subset
                    vector[i] = 1;
                    totalValue += data[i].value;
                    totalVolume += data[i].volume;
                    return 1;
                }
                // remove item i from subset
                vector[i] = 0;
                recomputeTotalValue(); // to avoid numerical errors - it is crucial (proved by testing)
                totalVolume -= data[i].volume;
            }
            return 0;
        }

        std::vector<unsigned> getItemsIndices() const
        {
            std::vector<unsigned> retval;
            for(unsigned i = 0; i < vector.size(); ++i)
            {
                if(vector[i])
                    retval.push_back(i);
            }
            return retval; // in C++11 compiler can optimize returning vector and move it instead of copying
        }

        const std::vector<bool>& getVector() const { return vector; }
        double getTotalValue() const { return totalValue; }
        unsigned long long getTotalVolume() const { return totalVolume; }

        void set(const std::vector<bool> &v, double value, unsigned long long volume)
        {
            vector = v;
            totalValue = value;
            totalVolume = volume;
        }

    private:
        const knapsack_problem::Data &data;
        std::vector<bool> vector; // vector[i]==true means than item number i is in the subset
        double totalValue;
        unsigned long long totalVolume;

        void recomputeTotalValue()
        {
            totalValue = 0.0;
            for(unsigned i = 0; i < vector.size(); ++i)
            {
                if(vector[i])
                    totalValue += data[i].value;
            }
        }
    };
}

void knapsack_problem::BruteForceSolution::solve(const Data &data)
{
    chosenItemsIndices.clear();
    ItemSubset subset(data);
    ItemSubset optimalSubset = subset;
    while(subset.nextSubset()) // find optimal subset - with largerst value
    {
        if(subset.getTotalVolume() > data.getKnapsackCapacity())
            continue;
        if(subset.getTotalValue() > optimalSubset.getTotalValue())
            optimalSubset.set(subset.getVector(), subset.getTotalValue(), subset.getTotalVolume());
    }
    totalValue = optimalSubset.getTotalValue();
    chosenItemsIndices = optimalSubset.getItemsIndices();
}

unsigned long long knapsack_problem::BruteForceSolution::getExpectedComplexity(unsigned n) const
{
    unsigned long long x = 1;
    return x << n; // 2^n
}

// Function that makes BruteForceSolution comparable with other solutions
void knapsack_problem::BruteForceSolution::unify(const knapsack_problem::Data &d)
{
    // Indices are already sorted in this solution.
    // Recompute totalValue (because result of floating-point operations depends on the order in which they are performed)
    totalValue = 0.0;
    for(unsigned i : chosenItemsIndices)
        totalValue += d[i].value;
}
