#include "dynamic_solution.h"
#include "data.h"
#include <algorithm>
#ifndef NDEBUG
    #include <iomanip>
#endif

void knapsack_problem::DynamicSolution::solve(const Data &data)
{
    chosenItemsIndices.clear();
    Array2d max_values_array(data.getNumberOfItems() + 1, data.getKnapsackCapacity() + 1);
    for(unsigned items = 1; items <= data.getNumberOfItems(); ++items) // fill maxValuesArray row by row
    {
        Item item = data[items - 1];
        for(unsigned cap = 1; cap <= data.getKnapsackCapacity(); ++cap)
        {
            if(item.volume > cap) // item does not fit into the knapsack - optimal solution is the same as if there was no additional item
                max_values_array(items, cap) = max_values_array(items - 1, cap);
            else // choose better value - without including the new item or with including it
                max_values_array(items, cap) = std::max(max_values_array(items - 1, cap), max_values_array(items - 1, cap - item.volume) + item.value);
        }
    }
    totalValue = max_values_array(data.getNumberOfItems(), data.getKnapsackCapacity());
    unsigned cap = data.getKnapsackCapacity();
    for(unsigned items = data.getNumberOfItems(); items > 0; --items)
    {
        // Below - I'm aware of numerical errors, but in this case values must be ideally equal if item has not been chosen
        // because in loop above there is a simple assignment: max_values_array(items, cap) = max_values_array(items - 1, cap)
        if(max_values_array(items, cap) != max_values_array(items - 1, cap))
        {
            chosenItemsIndices.push_back(items - 1); // -1 because of 0-based indexing
            cap -= data[items - 1].volume;
        }
    }
}

unsigned long long knapsack_problem::DynamicSolution::getExpectedComplexity(unsigned n) const
{
    return n*n;
}

// Function that makes DynamicSolution comparable with other solutions
void knapsack_problem::DynamicSolution::unify(const knapsack_problem::Data &d)
{
    // DynamicSolution returns indices in reversed order - sort by reversing:
    std::reverse(chosenItemsIndices.begin(), chosenItemsIndices.end());
    // Recompute totalValue (because result of floating-point operations depends on the order in which they are performed)
    totalValue = 0.0;
    for(unsigned i : chosenItemsIndices)
        totalValue += d[i].value;
}

knapsack_problem::DynamicSolution::Array2d::Array2d(unsigned rows, unsigned cols)
    : rowLength(cols), values((size_t)rows*(size_t)cols, 0.0f)
{}

double &knapsack_problem::DynamicSolution::Array2d::operator()(unsigned row, unsigned column)
{
#ifdef NDEBUG
    return values[(size_t)row*rowLength + (size_t)column];
#else
    return values.at((size_t)row*rowLength + (size_t)column);
#endif
}

#ifndef NDEBUG
void knapsack_problem::DynamicSolution::Array2d::print(std::ostream &os)
{
    unsigned cols = rowLength;
    unsigned rows = values.size() / rowLength;
    for(unsigned row = 0; row < rows; ++row)
    {
        for(unsigned col = 0; col < cols; ++col)
            os << std::setw(11) << operator()(row, col) << " ";
        os << "\n";
    }
}
#endif
