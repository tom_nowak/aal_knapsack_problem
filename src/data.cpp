#include "data.h"
#include "parser.h"
#include <string>
#include <sstream>
#include <stdexcept>
#include <chrono>

knapsack_problem::Data::Data(std::istream &input)
{
    Parser parser;
    if(!parser.getLine(input))
        throw std::invalid_argument("Error - knapsack capacity not provided");
    parser.readExactlyOneNumber(knapsackCapacity);
    while(parser.getLine(input))
    {
        Item tmp;
        parser.readNumbers(tmp.value, tmp.volume);
        push_back(tmp);
    }
}

knapsack_problem::Data::Data(unsigned number_of_items, unsigned knapsack_capacity, unsigned min_item_volume, unsigned max_item_volume)
    : knapsackCapacity(knapsack_capacity)
{
    // values - arbitrary choice (they are not important for performance tests)
    std::uniform_real_distribution<double> values(std::numeric_limits<double>::epsilon(), static_cast<double>(number_of_items));
    if(min_item_volume > max_item_volume)
        throw std::invalid_argument("Data constructor - min_item_volume cannot be greater than max_item_volume");
    // volumes - according to given data
    std::uniform_int_distribution<unsigned> volumes(min_item_volume, max_item_volume);
    for(unsigned i = 0; i < number_of_items; ++i)
    {
        Item tmp;
        tmp.value = values(randomGenerator);
        tmp.volume = volumes(randomGenerator);
        push_back(tmp);
    }
}

void knapsack_problem::Data::save(std::ostream &output) const
{
    output << knapsackCapacity << "\n";
    for(const Item &it : *this)
        output << it.value << " " << it.volume << "\n";
    output << "\n";
}

unsigned knapsack_problem::Data::getKnapsackCapacity() const
{
	return knapsackCapacity;
}

unsigned knapsack_problem::Data::getNumberOfItems() const
{
	return size();
}

const knapsack_problem::Item & knapsack_problem::Data::operator[](unsigned index) const
{
#ifdef NDEBUG
	return std::vector<Item>::operator[](index); // does not check range
#else
	return at(index); // throws expection if index is out of range
#endif
}

std::default_random_engine knapsack_problem::Data::randomGenerator(
    std::chrono::system_clock::now().time_since_epoch().count());
