#include "parser.h"
#include "data.h"
#include "brute_force_solution.h"
#include "dynamic_solution.h"
#include "performance_tester.h"
#include <memory> // unique_ptr
#include <iostream> // cin, cout
#include <cstring> // strcmp

namespace
{
    void helpGeneral(const char *program_name)
    {
        std::cout <<
            "Usage: " << program_name << " <command> <<arguments>>\n\n"
            "Possible commands <and their arguments>:\n"
            "gen <number of items> <knapsack capacity> <min item volume> <max item volume>\n"
            "solve <algorithm name>\n"
            "test <algorithm name> <start n> <step n> <W coefficient> <instances per test> <min item volume> <max item volume> <total time>\n\n"
            "Type one of the commands without arguments for more info.\n\n";
        std::exit(1);
    }

    void helpGen(const char *program_name)
    {
        std::cout <<
            "Usage: " << program_name << " gen <number of items> <knapsack capacity> <min item volume> <max item volume>\n\n"
            "<number of items>, <knapsack capacity> - positive integers\n"
            "<min item volume>, <max item volume> - positive integers, min <= max\n\n"
            "Generate problem instance and write to stdandard output - can be redirected: >file):\n"
            "first line: <knapsack capacity>\n"
            "other lines - one item per line: <item value> <item volume>\n"
            "the last line is empty\n\n"
            "Example usage:\n"
            << program_name << " gen 15 10 5 11 >output.txt\n\n";
        std::exit(1);
    }

    void executeGen(int argc, const char **argv)
    {
        if(argc != 6)
            helpGen(argv[0]);
        unsigned numberOfItems, knapsackCapacity, minItemVolume, maxItemVolume;
        Parser().readNumbersFromStringArray(argv + 2, numberOfItems, knapsackCapacity, minItemVolume, maxItemVolume);
        knapsack_problem::Data data(numberOfItems, knapsackCapacity, minItemVolume, maxItemVolume);
        data.save(std::cout);
    }

    void helpSolve(const char *program_name)
    {
        std::cout <<
            "Usage: " << program_name << " solve <algorithm name>\n\n"
            "Possible algorithms:\n"
            "brute - brute force solution\n"
            "dyn - dynamic solution\n\n"
            "Input data (from standard input - can be redirected: <file):\n"
            "first line of data: <knapsack capacity>\n"
            "other lines - one item per line: <item value> <item volume>\n"
            "the last line should be empty \n\n"
            "Output data (written to stdout - can be redirected: >file):\n"
            "max value: <max value>\n"
            "chosen items:\n"
            "<0-based index in data vector> (in each line separate)\n\n"
            "Example usage:\n"
            << program_name << " solve dyn <data.txt >output.txt\n"
            "with data.txt containing:\n"
            "11\n"
            "4.4 7\n"
            "3.2 6\n"
            "2.7 4\n\n";
        std::exit(1);
    }

    void executeSolve(int argc, const char **argv)
    {
        std::unique_ptr<knapsack_problem::Solution> solution;
        if(argc != 3)
            helpSolve(argv[0]);
        if(strcmp(argv[2], "brute") == 0)
            solution.reset(new knapsack_problem::BruteForceSolution);
        else if(strcmp(argv[2], "dyn") == 0)
            solution.reset(new knapsack_problem::DynamicSolution);
        else
            helpSolve(argv[0]);
        knapsack_problem::Data data(std::cin);
        solution->solve(data);
        solution->unify(data);
        solution->save(std::cout);
    }

    void helpTest(const char *program_name)
    {
        std::cout <<
            "Usage: " << program_name <<
            " test <algorithm name> <start n> <step n> <W coefficient> <instances per test> <min item volume> <max item volume> <total time>\n\n"
            "Generates <instances per test> for growing n (from <start n>, by <step n>) and measures execution time.\n"
            "Stops after <total time> seconds (at least - time is checked after each iteration).\n\n"
            "Possible algorithms (the same as in solve):\n"
            "brute - brute force solution\n"
            "dyn - dynamic solution\n\n"
            "<W coefficient> - floating point number: knapsack capacity = number of items * coefficient\n"
            "<min item volume> and <max item volume> are constant for each n.\n\n"
            "Output data (written to stdout - can be redirected: >file):\n"
            "Array containing (for each row):\n"
            "n t(n) q(n)\n"
            "where t(n) - average execution time (in milliseconds)\n"
            "q(n) - coefficient, should be about 1 if expected complexity matches with measured time\n\n"
            "Example usage:\n"
            << program_name << " test dyn 1000 100 0.5 10 1 50 100\n\n";
        std::exit(1);
    }

    void executeTest(int argc, const char **argv)
    {
        std::unique_ptr<knapsack_problem::Solution> solution;
        if(argc != 10)
            helpTest(argv[0]);
        if(strcmp(argv[2], "brute") == 0)
            solution.reset(new knapsack_problem::BruteForceSolution);
        else if(strcmp(argv[2], "dyn") == 0)
            solution.reset(new knapsack_problem::DynamicSolution);
        else
            helpTest(argv[0]);

        PerformanceTester pt;
        Parser().readNumbersFromStringArray(argv + 3, pt.minN, pt.stepN, pt.coefficientW, pt.instancesPerTest,
                                            pt.minItemVolume, pt.maxItemVolume, pt.minTotalTime);
        pt.testPerformance(solution.get());
        pt.saveOutput(std::cout);
    }
}

int main(int argc, const char **argv)
{
    try
    {
        std::ios_base::sync_with_stdio(false); // do not synchronize for performance (I'm using only cin, not stdin)
        if(argc < 2)
            helpGeneral(argv[0]);
        if(strcmp(argv[1], "gen") == 0)
            executeGen(argc, argv);
        else if(strcmp(argv[1], "solve") == 0)
            executeSolve(argc, argv);
        else if(strcmp(argv[1], "test") == 0)
            executeTest(argc, argv);
        else
            helpGeneral(argv[0]);
        return 0;
    }
    catch(const std::exception &e)
    {
        std::cerr << "exception caught: " << e.what() << "\n";
        return -1;
    }
}
