#include "performance_tester.h"
#include "solution.h"
#include "data.h"
#include <iomanip>
#include <chrono>

const std::vector<PerformanceTester::Output> &PerformanceTester::getOutput()
{
    return output;
}

void PerformanceTester::testPerformance(knapsack_problem::Solution *solution)
{
    auto total_start = std::chrono::steady_clock::now();
    std::vector<knapsack_problem::Data> data; // will store all data instances for particular (n, W)
    data.reserve(instancesPerTest);
    for(unsigned n = minN;; n += stepN)
    {
        unsigned W = std::ceil((float)n * coefficientW);
        for(unsigned i = 0; i < instancesPerTest; ++i)
            data.emplace_back(n, W, minItemVolume, maxItemVolume); // note that creating data is not included in time measurement
        auto start = std::chrono::steady_clock::now();
        for(const knapsack_problem::Data &d : data)
            solution->solve(d);
        auto end = std::chrono::steady_clock::now();
        unsigned duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
        data.clear();
        output.emplace_back(n, duration/instancesPerTest);
        auto total_end = std::chrono::steady_clock::now();
        if(std::chrono::duration_cast<std::chrono::seconds>(total_end - total_start).count() >= minTotalTime)
            break;
    }
    setQCoeffinientsInOutput(solution);
}

void PerformanceTester::saveOutput(std::ostream &os) const
{
    const unsigned n_field_width = std::to_string(output.back().n).length() + 4; // for elegant printing
    const unsigned t_field_width = std::to_string(output.back().t).length() + 4;
    const unsigned q_field_width = 12; // max float width + 4
    os << std::setw(n_field_width) << std::left << "n" <<
          std::setw(t_field_width) << std::left << "t(n)" <<
          std::setw(q_field_width) << std::left << "q(n)" << "\n";
    for(const PerformanceTester::Output &out : output)
    {
        os << std::setw(n_field_width) << std::left << out.n <<
              std::setw(t_field_width) << std::left << out.t <<
              std::setw(q_field_width) << std::left << out.q << "\n";
    }
}

void PerformanceTester::setQCoeffinientsInOutput(const knapsack_problem::Solution *solution)
{
    #ifndef NDEBUG
        if(output.size() == 0)
            throw std::logic_error("setQCoeffinientsInOutput impoutputsible - there is no output");
    #endif
    float t_median, T_median;
    // sequence of n is an arithmetic series, so n_median is in the middle of output vector:
    if(output.size() % 2)
    {
        unsigned n_median = output[(output.size() - 1)/2].n; // -1 because of 0-based indexing
        t_median = static_cast<float>(output[(output.size() - 1)/2].t);
        T_median = static_cast<float>(solution->getExpectedComplexity(n_median));
    }
    else
    {
        unsigned n1 = output[output.size()/2 ].n;
        unsigned n2 = output[output.size()/2 - 1].n;
        t_median = static_cast<float>( output[output.size()/2].t + output[output.size()/2 - 1].t )/2.0f;
        T_median = static_cast<float>( solution->getExpectedComplexity(n1) + solution->getExpectedComplexity(n2) )/2.0f;
    }
    float c = T_median / t_median;
    for(Output &out : output)
        out.q = out.t * c / solution->getExpectedComplexity(out.n);
}
