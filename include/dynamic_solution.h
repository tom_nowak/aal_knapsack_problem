#pragma once
#include "solution.h"
#ifndef NDEBUG
    #include <iostream>
#endif

namespace knapsack_problem
{
    class Data;

    class DynamicSolution : public Solution
    {
    public:
        virtual void solve(const Data &data);
        virtual unsigned long long getExpectedComplexity(unsigned n) const;
        virtual void unify(const Data &d);

    protected:
        class Array2d
        {
        public:
            Array2d(unsigned rows, unsigned cols);
            double& operator()(unsigned row, unsigned column);
            #ifndef NDEBUG
                void print(std::ostream &os);
            #endif

        private:
            size_t rowLength;
            std::vector<double> values;
        };
    };
}
