#pragma once
#include <sstream>
#include <limits>
#include <stdexcept>

// Parser is quite generic. The only two non-generic things are: the parser accepts only non-negative numbers and there are
// function overloads for types: unsigned (checking if is is nonzero) and float (checking if it is greater than epsilon).
class Parser
{
public:
    // Set value of input sequence using str.
    void setInputString(const std::string &str)
    {
        iss.clear();
        iss.str(str);
    }

    // Set value of input sequence using line from input. Returns false if empty line was read, true otherwise.
    bool getLine(std::istream &input)
    {
        std::string string;
        std::getline(input, string);
        if(!input)
            return 0;
        iss.clear();
        iss.str(string);
        return !string.empty();
    }

    template<typename T>
    void readExactlyOneNumber(T &number)
    {
        readNumbers(number);
        if(iss.rdbuf()->in_avail() != 0)
            throw std::invalid_argument("Parser - input seqence after number should be empty");
    }

    template<typename T>
    void readNumbers(T &number)
    {
        if(!isdigit(iss.peek()))
            throw std::invalid_argument("Parser - first character is not a digit");
        if(!(iss >> number))
            throw std::invalid_argument("Parser - failed to read number");
    }

    void readNumbers(unsigned &number) // overload - to check if number is nonzero
    {
        if(!isdigit(iss.peek()))
            throw std::invalid_argument("Parser - first character is not a digit");
        if(!(iss >> number))
            throw std::invalid_argument("Parser - failed to read number");
        if(number == 0)
            throw std::invalid_argument("Parser - expected nonzero integer");
    }

    void readNumbers(float &number) // overload - to check if number is greater than epsilon
    {
        if(!isdigit(iss.peek()))
            throw std::invalid_argument("Parser - first character is not a digit");
        if(!(iss >> number))
            throw std::invalid_argument("Parser - failed to read number");
        if(number <= std::numeric_limits<float>::epsilon())
            throw std::invalid_argument("Parser - expected floating point number greater than epsilon");
    }

    template<typename T, typename ...Args>
    void readNumbers(T &number, Args &...args)
    {
        readNumbers(number);
        if(iss.get() != ' ')
            throw std::invalid_argument("Parser - expected space after number");
        readNumbers(args...);
        if(iss.rdbuf()->in_avail() != 0)
            throw std::invalid_argument("Parser - input seqence after number should be empty");
    }

    template<typename T>
    void readNumbersFromStringArray(const char **string_array, T &number) // string_array must be big enough
    {
        setInputString(*string_array);
        readExactlyOneNumber(number);
    }

    template<typename T, typename ...Args>
    void readNumbersFromStringArray(const char **string_array, T &number, Args &...args) // string_array must be big enough
    {
        readNumbersFromStringArray(string_array, number);
        readNumbersFromStringArray(string_array + 1, args...);
    }

private:
    std::istringstream iss;
};
