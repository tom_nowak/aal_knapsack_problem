#pragma once
#include <ostream>
#include <vector>

namespace knapsack_problem
{
    class Solution; // forward declaration
}

class PerformanceTester
{
public:
    struct Output
    {
        unsigned n; // test case size
        unsigned long long t; // average execution time (in milliseconds)
        float q; // coefficient, should be about 1 if expected complexity matches with measured time
        // let T(n) be complexityCoefficient(n) (for given algorithm)
        // then q = ( t(n) / T(n) ) * ( T(n_median) / t(n_median) )

        Output(unsigned nn, unsigned tt)
            : n(nn), t(tt) // q uninitialized - will be changed later
        {}
    };

    // Public parameters to avoid getters/setters boilerplate code
    unsigned minN; // minimal problem size (n)
    unsigned stepN; // n is changed by stepN in each iteration
    float coefficientW; // W = n * coefficientW
    unsigned instancesPerTest; // for each n tests are repeated to minimize results of data randomness
    unsigned minItemVolume;
    unsigned maxItemVolume;
    unsigned minTotalTime; // (in seconds) if total execution time reaches this time, tests end

    PerformanceTester() {}
    const std::vector<Output>& getOutput();
    void testPerformance(knapsack_problem::Solution *solution);
    void saveOutput(std::ostream &os) const;

private:
    std::vector<Output> output;
    void setQCoeffinientsInOutput(const knapsack_problem::Solution *solution);
};
