#pragma once
#include <vector>
#include <ostream>
#include <iomanip>

namespace knapsack_problem
{
    class Data;

    // Abstract class providing common interface for classes solving knapsack problem.
	class Solution
	{
	public:
        Solution() : totalValue(0.0) {}
        virtual ~Solution() {}
        virtual void solve(const Data &data) = 0;
        const std::vector<unsigned>& getChosenItemsIndices() const { return chosenItemsIndices; }

        // Theoretical complexity of number of items (n) - assuming that knapsack capacity is const*n
        virtual unsigned long long getExpectedComplexity(unsigned n) const = 0;

        // unify is needed to make comparing different algorithms' results possible:
        // - items indices must be listed in the same (sorted) order
        // - totalValues must be computed in the same way (result of floating-point operations depends on the order in which they are performed)
        virtual void unify(const Data &d) = 0;

        void save(std::ostream &output) const
        {
            output << "max value: " << std::setprecision(5) << totalValue << "\nchosen items:\n";
            for(unsigned number : chosenItemsIndices)
                output << number << "\n";
        }

	protected:
		std::vector<unsigned> chosenItemsIndices;
        double totalValue;
	};
}
