#pragma once
#include <istream>
#include <ostream>
#include <vector>
#include <random>

namespace knapsack_problem
{
	struct Item
	{
        double value;
		unsigned volume;
	};

	class Data : protected std::vector<Item>
	{
	public:
		Data(std::istream &input); // create from file/standard input
        Data(unsigned number_of_items, unsigned knapsack_capacity, unsigned min_item_volume, unsigned max_item_volume); // create random data
        void save(std::ostream &output) const; // serialized data can be used in constructor Data(std::istream &input)
		unsigned getKnapsackCapacity() const;
		unsigned getNumberOfItems() const;
		const Item& operator[](unsigned index) const;

	protected:
		unsigned knapsackCapacity;
        static std::default_random_engine randomGenerator;
	};
}
