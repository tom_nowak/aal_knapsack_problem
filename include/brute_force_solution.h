#pragma once
#include "solution.h"

namespace knapsack_problem
{
    class Data;

    class BruteForceSolution : public Solution
    {
    public:
        virtual void solve(const Data &data);
        virtual unsigned long long getExpectedComplexity(unsigned n) const;
        virtual void unify(const Data &d);
    };
}
