## Tomasz Nowak - Projekt z przedniotu Analiza Algorytmów  

### 1. Opis problemu.  
Mamy n przedmiotów o objętościach całkowitych v_i, i=1...n. Wartości tych przedmiotów to, odpowiednio p_i>0. Należy znaleźć zbiór przedmiotów mieszczących się do plecaka o objętości W, charakteryzujący się największą sumaryczną wartości.  


### 2. Wymagane oprogramowanie i sposób kompilacji.
Wymagania: CMake, kompilator C++ zgodny ze standardem C++11.  
  
Kompilacja (Linux):  
```
mkdir build  
cd build  
cmake ..  
make
```
Na innych systemach - CMake to przenośne narzędzie, utworzy projekt zgodny z wymaganiami danego środowiska (np. projekt VisualStudio na Windowsie).  

Do dodatkowych skryptów w tests (patrz punkt 4.2.) wymagane: python, numpy, matplotlib.  


### 3. Uruchomienie programu. 
Program umożliwia wykonanie w trzech trybach:  

#### 3.1. Generacja danych na standardowe wyjście (można je przekierować do pliku):
<nazwa programu> gen <liczba przedmiotów> <objętość plecaka> <minimalna objętość przedmiotu> <maksymalna objętość przedmiotu>  
Wartości przedmiotów są losowane - liczby dodatnie (niekoniecznie całkowite) nie większe niż <liczba przedmiotów> - wybór arbitralny (ponieważ rozkład objętości nie ma wpływu na zachowanie się algorytmu). Pozostałe parametry są tworzone zgodnie z ich nazwami.  

Format danych wyjściowych:  
<objętość plecaka>  
<wartość przedmiotu 0> <objętość przedmiotu 0>  
<wartość przedmiotu 1> <objętość przedmiotu 1>  
...  

Przykład użycia:  
./knapsack gen 15 10 5 11 >data.txt  

#### 3.2. Rozwiązanie instancji problemu:  
<nazwa programu> solve <nazwa algorytmu>  
Dostępne algorytmy:  
brute - brute force - sprawdzenie wszystkich możliwych podzbiorów zbioru n-elementowego  
dyn - programowanie dynamiczne  

Dane wejściowe - ze standardowego wejścia (można je przekierować z pliku). Format danych - taki sam jak generowany przez gen.  

Wyjście - na standardowe wyjście. Format danych wyjściowych:  
max value: <maksymalna wartość przedmiotów w plecaku>  
chosen items:  
<numer>  
...  

Przykład użycia:  
./knapsack solve dyn <data.txt >output.txt  

#### 3.3. Test złożoności:  
<nazwa programu> test <nazwa algorytmu> <początkowe n> <krok n> <współczynnik W> <liczba powtórzeń dla każdego n> <minimalna objętość przedmiotu> <maksymalna objętość przedmiotu> <czas>  
Program generuje dane dla rosnącego n (począwszy od <początkowe n>, co <krok n>) i mierzy czas wykonania wybranego algorytmu. Po każdym teście jest sprawdzany całkowity <czas> (w sekundach), jaki upłynął od początku. Po przekroczeniu podanego czasu, działanie programu kończy się.  

Nieoczywisty parametr <współczynnik W> - aby umożliwić jednowymiarową analizę problemu, objętość plecaka (W) jest uzależniana liniowo od liczby przedmiotów (n) - z podanym współczynnikiem proporcjonalności. Oprócz obliczania średniego czasu wykonania dla każdego n, program wyznacza współczynnik q(n) będący miarą zgodności teoretycznej złożoności z czasem wykonania (1 - zgodna, mniejszy niż 1 - występuje przeszacowanie, większy niż 1 - występuje niedoszacowanie).  

Dane wyjściowe - na standardowe wyjście: rozmiar problemu, średni czas wykonania w milisekundach, współczynnik złożoności:  
n t(n) q(n)  
<n t(n) q(n) - dla każdego n, dla którego przeprowadzono test>  

Przykład użycia:  
./knapsack test dyn 1000 100 0.5 10 1 50 100  



### 4. Pliki w kodzie źródłowym.  

#### 4.1. Kod głównego programu - knapsack:  

main.cpp - obsługa konsolowego interfejsu i wywoływanie poszczególnych sposobów wywołania programu  
solution.h - klasa abstrakcyjna Solution, będąca interfejsem dla rozwiązań problemu plecakowego  
brute_force_solution.h, brute_force_solution.cpp - klasa BruteForceSolution dziedzicząca po Solution - do rozwiązania algorytmem "brute"  
dynamic_solution.h, dynamic_solution.cpp - analogicznie jak wyżej - klasa DynamicSolution - do rozwiązania algorytmem "dyn"  
data.h, data.h - klasa Data generująca i przechowująca dane (argumenty dla algorytmów dziedziczących po Solution)  
performance_tester.h, performance_tester.cpp - klasa PerformanceTester, wykonująca testy wydajności danego algorytmu  
parser.h - klasa zawierająca szablony do uniwersalnego (prostego) wczytywania danych; przy niepoprawnych formacie danych rzucane są wyjątki

#### 4.2. Inne pliki:  
katalog doc - dokumentacja  
katalog tests - testy:  
performance_test.sh - uruchamia kilka testów wydajności; nie jest to plik przenośny, ale wywołania można łatwo skopiować np. na Windowsa  
random_test.py - uruchamia testy losowe i porównuje, czy dwa różne algorytmy (brute, dyn) zwróciły dokładnie to samo  
plot.py - na podstawie danych w tests/data tworzy wykresy i zapisuje w tests/images  
  
