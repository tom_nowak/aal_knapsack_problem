\documentclass{article}
\usepackage{polski}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}

\title{%
  Analiza Algorytmów - problem plecakowy \\
  \large Prowadzący - dr hab. inż. Paweł Wawrzyński
}
\author{Tomasz Nowak}
\date{}

\begin{document}

\maketitle

\section{Treść zadania}
Mamy $n$ przedmiotów o objętościach całkowitych $v_i > 0$, $i=1..n$. Wartości tych przedmiotów to, odpowiednio $p_i>0$. Należy znaleźć zbiór przedmiotów mieszczących się do plecaka o objętości $W$, charakteryzujący się największą sumaryczną wartością.

\section{Użyte algorytmy}

\subsection{Rozwiązanie \textit{brute force}}
\subsubsection{Opis}
Sprawdzamy wszystkie możliwe podzbiory zbioru przedmiotów i wśród nich wybieramy ten o największej łącznej wartości. Pozdziór jest reprezentowany przez ciąg binarny długości $n$ ($i$-ty przedmiot jest w rozważanym pozbiorze gdy $i$-ty element ciągu jest równy 1), a przejście do kolejnego podzbioru to dodanie 1 do liczbowej interpretacji ciągu.
\subsubsection{Ocena złożoności}
Złożoność obliczeniowa: $O(2^n)$ (sprawdzenie $2^n$ podzbiorów). Złożoność pamięciowa: $O(n)$ (ale z bardzo małym współczynnikiem) - przechowywanie ciągów binarnych reprezentujących: aktualnie sprawdzany pozdbiór i pozdbiór z dotychczas największą znalezioną wartością.

\subsection{Programowanie dynamiczne}
\subsubsection{Opis - znajdowanie maksymalnej wartości plecaka}
Oznaczmy przez $f(m, w)$ maksymalną wartość $m$ przedmiotów ($m=0..n$) w plecaku o objętości $w$ ($w=0..W$). Problem znalezienia wartości funkcji f można podzielić na dwa podproblemy:
\begin{itemize}
    \item przedmiotu numer $m$ nie ma wśród wybranych przedmiotów - wtedy: $$f(m, w) = f(m-1, w)$$
    \item przedmiot numer $m$ jest wśród wybranych przedmiotów - wtedy: $$f(m, w) = f(m-1, w-v_m) + p_m$$
\end{itemize}
Przypadek drugi może zachodzić tylko wtedy gdy przedmiot $m$ mieści się w plecaku, tj. $v_m<=w$. Jeśli mamy możliwość wyboru - wybieramy większą wartość (dążymy do maksymalizacji f). Czyli można zapisać rekurencyjnie:
\begin{itemize}
    \item $if(v_m>w)$ $f(m, w) = f(m-1, w)$
    \item $else$ $f(m, w) = max(f(m-1, w), f(m-1, w-v_m) + p_m)$
\end{itemize}
Implementacja rekurencyjna nie byłaby wydajna, ponieważ te same podproblemy byłyby obliczane wielokrotnie. Dlatego należy zastosować podejście programowania dynamicznego - tworzymy tablicę wartości funkcji $f$ dla wszystkich możliwych argumentów - tablica o $n+1$ wierszach i $W+1$ kolumnach (+1 ze względu na to że rozważając np. $f(1,...)$, odwołujemy się do $f(0,...)$ - dla uproszczenia implementacji uwzględniono wartości $f$ dla argumentów 0). Wiersz (0,...) i kolumnę (...,0) wypełniamy zerami. Następnie, iteracyjnie obliczamy wartości $f$ kolejno dla każdego wiersza zgodnie z podanymi wyżej równaniami rekurencyjnymi. Na końcu w ostatniej kolumnie w ostatnim wierszu jest poszukiwana maksymalna wartość: $f(n,W)$.
\subsubsection{Opis - wybór przedmiotów}
Pozostaje znaleźć, które przedmioty zostały wybrane - jeśli $f(m,w)$ jest rozwiązaniem jednego z podproblemów będących częścią rozwiązania optymalnego, to przedmiot numer $m-1$ został wybrany gdy $f(m,w) \neq f(m-1,w)$ (wynika to z procedury wyboru przedmiotu). Szukanie wybranych przedmiotów zaczynamy od $m=n$ i $w=W$, w każdej iteracji pomniejszamy $m$ o 1, a $w$ pomniejszamy o $p_{m-1}$ przy dodawaniu przedmiotu numer $m-1$. Po osiągnięciu $m=0$, znaleźliśmy już wszystkie przedmioty - problem został rozwiązany.
\subsubsection{Ocena złożoności}
Mamy tablicę o wymiarach $n$ na $W$ i iterujemy po wszystkich jej elementach, więc złożoność obliczeniowa i pamięciowa: $O(nW)$.

\section{Opis sposobów uruchamiania i struktury programu}
Zgodnie z wymaganiami projektu, sposób uruchamiania programu, a także podział na pliki został opisany w pliku README.md. Nie umieszczono tutaj tego opisu aby nie duplikować informacji. Ponadto, uruchomienie programu bez argumentów / z błędnymi argumentami powoduje wyświetlenie informacji na temat obsługiwanych argumentów.

\section{Testowanie poprawności}
\subsection{Testowanie intuicyjne}
Uruchomiano program dla trywialnych danych (dla których możliwe jest sprawdzenie "ręczne") i dla danych z przykładowych rozwiązań problemu plecakowego dostępnych w Internecie (dla których znane było rozwiązanie).
\subsection{Testowanie losowe}
Skrypt losuje parametry, które są przekazywane jako argumenty generatora danych. Następnie, dla tych samych danych wejściowych, uruchamiane są oba algorytmy rozwiązujące problem plecakowy. Porównywane jest ich wyjście, jeśli wyniki różnią się - testy są przerywane, a pliki z danymi, które spowodowały błąd - zapisywane. 
\newline{} \newline{}
W czasie testów losowych został wykryty problem błędów numerycznych (wartości przedmiotów $p_i$ nie muszą być całkowite) - dwa algorytmy zwracały różne wartości, a czasami nawet różne zbiory przedmiotów. Zmieniono sposób obliczania wartości przy przejściu do kolejnego podzbioru zbioru przedmiotów (w algorytmie \textit{brute force}) - łączna wartość jest zwiększana przy dodaniu kolejnego przedmiotu do podzbioru, ale jest obliczana od nowa przy usuwaniu przedmiotu z podzbioru, ponieważ wielokrotne dodawanie i odejmowanie tych samych liczb generowało błędy. Po wprowadzeniu tej zmiany, oba algorytmy zwracały te same przedmioty, ale inne łączne wartości. Wynika to z tego, że wynik dodawania liczb zmiennopozycyjnych zależy od kolejności, w jakiej wykonano działania. Aby wyniki działania obu algorytmów były porównywalne, przed wypisaniem wyniku łączna wartość jest obliczana od nowa - w ten sam sposób dla obu algorytmów. Po wprowadzeniu tej zmiany, oba algorytmy zawsze zwracały dokładnie taki sam wynik.

\section{Pomiary złożoności - wykresy}
Przeprowadzono pomiary czasu wykonania (wraz z badaniem zgodności złożoności teoretycznej z rzeczywistym czasem wykonania - parametr $q(n)$ - zgodnie z zaleceniami projektowymi). Poniższe wykresy przedstawiają średni czas (w milisekundach) rozwiązania problemu o zadanym rozmiarze - $t(n)$ i wspomniany współczynnik $q(n)$ w funkcji rozmiaru problemu ($n$). Aby umożliwić jednowymiarową analizę problemu, parametr $W$ został uzależniony liniowo od $n$ - nie zmienia to złożoności algorytmu \textit{brute force}, ale zmienia złożoność programowania dynamicznego na $O(n^2)$. Współczynnik proporcjonalności to liczba napisana po "W" w tytule wykresu. W przypadku programowania dynamicznego w tytule wykresu zapisano też "smallV" lub "largeV" w zależności od tego, z jakiego zbioru wartości losowane były $v_i$.
\subsection{Algorytm \textit{brute force}}
\includegraphics[width=\textwidth]{brute_W1.png}
\includegraphics[width=\textwidth]{brute_W400.png}
\subsection{Programowanie dynamiczne - wsp. wzrostu $W$ 1}
\includegraphics[width=\textwidth]{dyn_W1_smallV.png}
\includegraphics[width=\textwidth]{dyn_W1_largeV.png}
\subsection{Programowanie dynamiczne - wsp. wzrostu $W$ 100}
\includegraphics[width=\textwidth]{dyn_W100_smallV.png}
\includegraphics[width=\textwidth]{dyn_W100_largeV.png}
\subsection{Programowanie dynamiczne - wsp. wzrostu $W$ 100000}
\includegraphics[width=\textwidth]{dyn_W100000.png}

\section{Pomiary złożoności - wnioski}
\subsection{Algorytm \textit{brute force}}
Pomiary wykazały (w przybliżeniu) zgodność czasu wykonania programu ze złożonością teoretyczną. Niewielkie odstępstwo to rosnąca wartość współczynnika $q(n)$, co świadczy o niedoszacowaniu. Wynika ono prawdopodobnie z tego, że wraz ze zwiększaniem $n$, zwiększa się długość ciągu binarnego reprezentującego podzbiór zbioru przedmiotów - wydłuża się czas przetwarzania tego ciągu. Należy również zwrócić uwagę na niezależność czasu wykonania programu od $W$, co jest zgodne z teorią.
\subsection{Programowanie dynamiczne}
Pomiary dla różnych rozkładów objętości przedmiotów ($v_i$) mają bardzo podobne wyniki, natomiast istotne różnice wprowadza zmiana $W$ względem $n$. Dla małego współczynnika wzrostu W wyniki pomiarów zgadzają się z teorią. Jest niewielkie odstępstwo $q(n)$ - począwszy od pewnego rozmiaru problemu występuje niedoszacowanie. Zapewne jest to spowodowane tym, że przy większych rozmiarach problemu dane nie mieszczą się już w kieszeniach i występują opóźnienia związane z częstszymi odwołaniami procesora do dalszych warstw hierarchii pamięci. \newline{} \newline{}
Katastrofalne skutki złożoności pamięciowej daje się zaobserwować przy większym współczynniku wzrostu $W$. Począwszy od pewnej krytycznej wartości, czas wykonania instancji problemu znacznie wzrasta (niemal pionowa linia na wykresie). Jest to spowodowane tym, że brakuje miejsca w pamięci fizycznej, w związku z czym system operacyjny zmuszony jest przenosić strony pamięci na dysk twardy. Zużycie procesora (obserwowane w programie \textit{top}) spada do kilku \% - prawie cały czas wykonywane są procedury systemu operacyjnego. Gdy brakuje pamięci, algorytm programowania dynamicznego staje się praktycznie bezużyteczny. Należy jednak zwrócić uwagę, że przed osiągnięciem tej krytycznej złożoności pamięciowej czasy wykonania kolejnych instancji problemu są zgodne z teoretyczną złożonością ($q(n)$ jest w przybliżeniu stałe i równe 1). \newline{} \newline{}
\subsection{Porównanie algorytmów}
Mniejsza złożoność pamięciowa algorytmu \textit{brute force} i niezależność od $W$ pokazują, że dla specjalnie przygotowanych danych \textit{brute force} może działać lepiej niż programowanie dynamiczne - pomimo znacznie gorszej złożoności obliczeniowej. Jednak w praktyce bardzo trudno o takie dane, o czym świadczy porównanie wyników ostatniego pomiaru czasu wykonania programowania dynamicznego (punkt 5.4) z czasami wykonania \textit{brute force} (punkt 5.1) - nawet dla współczynnika wzrostu $W$ równego 100000 programowanie dynamiczne działało znacznie lepiej. Natomiast dla wartości $n$, przy której zaczynało brakować pamięci ($n=69$) algorytm \textit{brute force} jest już praktycznie niewykonalny (już dla $n=20$ jego średni czas wykonania wynosił ponad 100 sekund, a $2^{69} / 2^{20} > 10^{14}$).

\section{Próby poprawy programowania dynamicznego}
Można sprawdzać, czy występuje przypadek szczególny - wszystkie przedmioty mieszczą się w plecaku - wtedy złożoność wyniesie $O(n)$. Jednak w praktyce takie sprawdzanie jest mało skuteczne - zwykle rozważamy sytuację, w której można wybrać tylko część przedmiotów.
\newline{} \newline{}
Jeżeli celem algorytmu jest tylko znalezienie maksymalnej możliwej wartości (a nie przedmiotów), można zmniejszyć złożoność pamięciową do $O(W)$ - wystarczy przechowywać w 2-elementowej liście cyklicznej z całej tablicy programowania dynamicznego wiersz "aktualny" i "poprzedni".
\newline{} \newline{}
Podjęto próbę poprawienia szybkości działania algorytmu - zamiast iterować po każdym wierszu tablicy, na początku kopiujemy z poprzedniego wiersza wyrazy o indeksach $0..v_i-1$ (odpowiadają one komórkom tabeli, dla których przedmiot i tak się nie zmieści w plecaku). Jednak praktyczne testy wydajności pokazały, że ta implementacja działała wolniej - być może kompilator lepiej optymalizuje proste iterowanie w podstawowej implementacji algorytmu.
\newline{} \newline{}
Programowanie dynamiczne zyskuje na szybkości działania kosztem złożoności pamięciowej i, jak pokazały testy, ta złożoność pamięciowa dla dużych problemów jest krytyczna. Być może istnieje algorytm - modyfikacja programowania dynamicznego - znajdujący korzystny kompromis między złożonością czasową a obliczeniową.
\end{document}
